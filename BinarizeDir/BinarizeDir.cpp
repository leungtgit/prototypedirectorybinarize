// BinarizeDir.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <regex>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <experimental\filesystem>
#include <vector>
#include <string>
#include <sstream>
using namespace std;
using namespace cv;
namespace fs = experimental::filesystem;

int main(int argc, char* argv[])
{
    ostringstream oss;
    vector<string> fileNames;
    vector<String> fileFormats;
    string filetype;
    int userThreshold;

    if (argc < 2) {
        cout << "You need to supply the threshold value. Pick a number 0-255." << endl;
        exit(1);
    }

    //regex things
    string imageJpeg = ".*\.jpeg";
    string imageJpg = ".*\.jpg";
    string imagePng = ".*\.png";
    string imageBmp = ".*\.bmp";
    regex rxJpeg(imageJpeg, regex::icase);
    regex rxJpg(imageJpg, regex::icase);
    regex rxPng(imagePng, regex::icase);
    regex rxBmp(imageBmp, regex::icase);

    //get current working directory
    string cwd;
    oss << fs::current_path();
    cwd = oss.str();
    oss.str("");
    oss.clear();

    //get threshold and get an int out of it
    oss << argv[1];
    try {
        userThreshold = stoi(oss.str());
        oss.str("");
        oss.clear();
    }
    catch (...) {
        perror("Error in turning threshold into integer");
        return 2;
        //exit(2);
    }

    for (auto &p : fs::directory_iterator(fs::current_path())) {
        oss << p;
        if (regex_match(oss.str(), rxJpeg) ||
            regex_match(oss.str(), rxJpg)  ||
            regex_match(oss.str(), rxPng)  ||
            regex_match(oss.str(), rxBmp)     ) {
            fileNames.push_back(oss.str());
            cout << setw(120) << left << oss.str() << setw(30) << right << "added" << endl;
        }
        oss.str("");
        oss.clear();
    }


    sort(fileNames.begin(), fileNames.end());

    Mat image;
    Mat bw;

    bool bwExists = false;
    //iterate through directory and find out if BW exists as a directory
    for (auto it = fs::directory_iterator("BW"); it != fs::directory_iterator(); it++) {
        if (fs::is_directory(*it)) {
            bwExists = true;
        }
    }

    //create the directory if it doesn't exist
    if (!bwExists) {
        fs::create_directory("BW");
    }
    for (int i = 0; i < fileNames.size(); i++) {
        image = imread(fileNames[i], CV_LOAD_IMAGE_GRAYSCALE);
        threshold(image, bw, userThreshold, 255, THRESH_BINARY);
        //cut out the filename
        string fName = fileNames[i].substr(cwd.size() + 1, fileNames[i].find_last_of(".") - cwd.size() - 1);
        string outFile = cwd + "\\BW\\" + fName + "_BW.png";
        if(imwrite(outFile, bw)) {
            cout << "Error writing to " << outFile << endl; 
        }
        image.release();
        bw.release();
    }
    return 0;
}

