// BinarizeDirRelease.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <regex>                            //for recognizing file formats
#include <opencv2/core/core.hpp>            //for opencv binarization stuff
#include <opencv2/opencv.hpp>               //for opencv binarization stuff
#include <opencv2/imgproc/imgproc.hpp>      //for opencv binarization stuff
#include <experimental\filesystem>          //for grabbing directories
#include <vector>                           //for pushing on filtered files and stuff
#include <string>                           //for manipulating filepaths
#include <sstream>                          //for turning filesystem paths into strings
using namespace cv;
using namespace std;
namespace fs = experimental::filesystem;

//args
//0: first one was calling this program
//1: Destination directory for BW images
//2: Source directory. Will default to current working directory if none is supplied

int main(int argc, char* argv[])
{

    return 0;
}


//class for hiding the image search behind a simple function call
class ImageData {
public:
    void SearchForImages() {

    }
};

class FilePathing {
public:
    //userPath1 = output directory
    //userPath2 = input directory
    FilePathing(string userPath1, string userPath2) {
        rxJpeg = regex(imageJpeg, regex::icase);
        rxJpg = regex(imageJpg, regex::icase);
        rxPng = regex(imagePng, regex::icase);
        rxBmp = regex(imageBmp, regex::icase);
        //check output directory
        
    }


    string Pathing(string userPath, string defaultPath, bool useCwd) {
        if (userPath != "") {
            //check if default directory exists
            for (auto it = fs::directory_iterator(defaultPath); it != fs::directory_iterator(); it++) {
                if (fs::is_directory(*it))
                    return defaultPath;
            }
            //if no dir to use and can use cwd, use current path
            if ((defaultPath == "") && useCwd)
                return FilePathing::PathToString(fs::current_path());
            if
        }
    }

    string PathToString(fs::path pathIn) {
        oss << pathIn;                      //pass to ostringstream
        string returnString = oss.str();    //pass as string to var
        oss.str("");                        //empty oss
        oss.clear();                        //clear oss to remove any errors
        return returnString;
    }

    string FileName(string fullPath) {

    }

private:
    ostringstream oss;
    const string imageJpeg = ".*\.jpeg";
    const string imageJpg = ".*\.jpg";
    const string imagePng = ".*\.png";
    const string imageBmp = ".*\.bmp";
    string cwd;
    string outputDir;
    regex rxJpeg; 
    regex rxJpg; 
    regex rxPng; 
    regex rxBmp; 
};